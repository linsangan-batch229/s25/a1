//JSON
//JavaScript object notation
//a data format from server to webpage
/*
JSON - aptly named JS objects looks like a JSobject
however, the keys of a JSON object is surrounded by "",


{
    "key1": "valeu1",
    "key2": 2500
}

JSON is a string format that looks like a JS object

*/

let sampleJSON = `
    {
        "name" : "Keat",
        "age" : 20
    }

`;

console.log(sampleJSON);

//string to JS object
//JSON.parse() - will return the given JSON as a JS object
let sampleJSON1 = JSON.parse(sampleJSON);
console.log(sampleJSON1);

//JS object to JSON
//JSON.stringify() - return a given jsobject as a stringify
//json format string.

let us1 = {
    username: "knight123",
    password: "1234",
    age: 4
}

let s1 = JSON.stringify(us1);
console.log(s1);

//JSON to JS object ->  JSON.parse();
//JS object to JSON -> JSON.stringify();

//JSON array
//array of json in json format

let sampleArr = `
    [
        {
            "email": "json@gmail.com",
            "password" : "1234",
            "isAdmin" : false
        },
        {
            "email": "string@gmail.com",
            "password" : "12345",
            "isAdmin" : false
        }
    ]


`;

console.log(sampleArr);

let parseArr = JSON.parse(sampleArr); // does not mutate the original
console.log(parseArr);
console.log(parseArr.pop());

//turn the parseArr to into JSONand update the sampleArr JSON
//array
sampleArr = JSON.stringify(parseArr);
console.log(sampleArr);

//Database (JSON format) -> server (JSON format to JS object)
//-> prcess(task to do the data) -> turn the data back to JSON
//->client )Webpage

//DB as JSON FORMAT -> server -> JS FORMAT TO JS OBJECT sa server
// -> turn the data into JSON client -> web page

//dos and donts of JSON format

//do: add double quotes to your keys
//Do: add colon after each key
//dont: dont add excessive commans
//dont: dont fotget to close doble quotes, curcly braces
let sampleD = `
    {
        "email": "james@wewe.com",
        "password": "1234g",
        "balance": 50000
    }
`;
console.log(`---------------------------------`);
let s2 = JSON.parse(sampleD);
console.log(s2);    


//sublime text JavaScript to JSON